extends Control
class_name UI

onready var _line: TextureRect = $Line;
onready var _miniBoat: TextureRect = $MiniBoat;
onready var _tickTimer: Timer = $TickTimer;
onready var _end_overlay: ColorRect = $EndOverlay;
onready var _victory_label: Label = $EndOverlay/VictoryLabel;
onready var _failure_label: Label = $EndOverlay/FailureLabel;

signal game_finished;

var start_size: float;
var start_position_line: float;
var start_position_boat: float;
var game_duration: float;

func _ready() -> void:
	start_size = _line.rect_size.x;
	start_position_line = _line.rect_position.x;
	start_position_boat = _miniBoat.rect_position.x;
	
func start_countdown(time: float) -> void:
	game_duration = time;
	_tickTimer.start();

func fail_game() -> void:
	_end_overlay.visible = true;
	_failure_label.visible = true;
	_tickTimer.stop();
	
func win_game() -> void:
	_end_overlay.visible = true;
	_victory_label.visible = true;
	_tickTimer.stop();

func _on_TickTimer_timeout() -> void:
	_line.rect_position.x += start_size / game_duration;
	_line.rect_size.x -= start_size / game_duration;
	_miniBoat.rect_position.x += start_size / game_duration;

func _on_ReplayButton_pressed() -> void:
	get_tree().reload_current_scene();
