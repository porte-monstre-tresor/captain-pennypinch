extends Node2D
class_name Level

onready var crack_scene = preload("res://entities/Crack/Crack.tscn");
onready var submerged_shader = preload("res://entities/Boat/submerged_shader.tres");

# Children nodes
onready var _boatWalls: TileMap = $Boat/BoatWalls;
onready var _boat: Node2D = $Boat;
onready var _ui: UI = $CanvasLayer/UI;
onready var player: CaptainPennyPinch = $Boat/CaptainPennyPinch;
onready var _endTimer: Timer = $EndTimer;

# Types of wall cells
enum WallCellType {
	HULL = 0,
	WALL = 1,
	BREAKING= 2,
	BROKEN = 6,
	REPAIRED = 7,
	WINDOW = 5
}

var BreakingTimer = Timer.new();
var Rng = RandomNumberGenerator.new();

var cracks_in_ship = {};
var amount_of_water: float = 0;
var max_water: float = 60;
var game_running = true;

export var water_per_hole_per_sec: float = 0.5;
export(float, 0, 1, 0.05) var chance_to_break_per_timeout: float = 0.1;
export var initial_water_level: float = 55.0;
export var breaking_time: float = 5;
export var game_time: float = 60;

func _ready() -> void:
	# Breaking timeout management
	add_child(BreakingTimer);
	BreakingTimer.connect("timeout", self, "_on_breaking_timeout");
	BreakingTimer.one_shot = true;
	BreakingTimer.wait_time = breaking_time;
	BreakingTimer.start();
	
	# Game duration management
	_endTimer.wait_time = game_time;
	_endTimer.start();
	_ui.start_countdown(game_time);
	
	# Start the music
	$AudioStreamPlayer.play();

func _process(delta: float) -> void:
	_boat.position.y = 1 + 32 * amount_of_water / max_water;
	if (game_running):
		for crack in cracks_in_ship.values():
			if (crack.broken):
				# Update the amount of water in the ship
				amount_of_water += water_per_hole_per_sec * delta;
		
		# Update the shader parameters of the boat
		_boat.material.set_shader_param("water_level", initial_water_level - amount_of_water);
		_boat.material.set_shader_param("camera_view", get_viewport_transform());
		
		if (amount_of_water > max_water):
			stop_game();
			_ui.fail_game();

func _on_breaking_timeout() -> void:
	if (game_running):
		# Coin-toss every breaking cell to see if it breaks
		var breaking_cells = _boatWalls.get_used_cells_by_id(WallCellType.BREAKING);
		for cell_coords in breaking_cells:
			if (Rng.randf() < chance_to_break_per_timeout):
				_boatWalls.set_cellv(cell_coords, WallCellType.BROKEN);
				cracks_in_ship[cell_coords].cracks();
		
		# Convert one vanilla wall cell into a breaking cell
		var wall_cells = _boatWalls.get_used_cells_by_id(WallCellType.WALL);
		var random_cell_coords = wall_cells[Rng.randi_range(0, wall_cells.size()-1)];
		_boatWalls.set_cellv(random_cell_coords, WallCellType.BREAKING);
		
		# Place a crack of the wall
		var crack_instance = crack_scene.instance();
		crack_instance.material = submerged_shader;
		crack_instance.position = _boatWalls.map_to_world(random_cell_coords);
		crack_instance.cell_pos = random_cell_coords;
		
		# Prepare the signal to fix the crack
		crack_instance.connect("cell_repaired", self, "repair_cellv");
		
		# Register the crack and add it to the scene tree
		cracks_in_ship[random_cell_coords] = crack_instance;
		_boatWalls.add_child(crack_instance);
		
		if (_endTimer.time_left < (game_time/3.0)):
			BreakingTimer.wait_time = 1;
		elif (_endTimer.time_left < (2*game_time/3.0)):
			BreakingTimer.wait_time = 3;
		BreakingTimer.start();

func repair_cellv(cellv: Vector2) -> void:
	_boatWalls.set_cellv(cellv, WallCellType.REPAIRED);
	cracks_in_ship.erase(cellv);

func stop_game() -> void:
	game_running = false;
	player.game_running = false;

# Music loop
func _on_AudioStreamPlayer_finished() -> void:
	$AudioStreamPlayer.play();

# When the player fall out of the map
func _on_OuterRealm_body_entered(body: Node) -> void:
	game_running = false;
	player.game_running = false;
	_ui.fail_game();

func _on_EndTimer_timeout() -> void:
	stop_game();
	_ui.win_game();
