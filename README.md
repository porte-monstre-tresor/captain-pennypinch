# At Least the Ship was Cheap

Small game created for the Ludum Dare 50 with the theme "Delay the Inevitable". 

I used different assets to create the game, here are the links:
- Original tileset by Gustavo Vituri: https://gvituri.itch.io/ghost-ship
- Background by gamer247: https://gamer247.itch.io/cu-azul
- Font by Joe Brogers: https://joebrogers.itch.io/bitpotion
- Music by Abstraction: https://soundcloud.com/abstraction/ludum-dare-30-eighth-track?in=abstraction/sets/ludum-dare-challenge 

Controls:
- Left/Right/Up arrows to move left/right/jump
- Down arrow to fall through
- Left click to repair