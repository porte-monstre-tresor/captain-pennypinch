extends KinematicBody2D
class_name CaptainPennyPinch

#region Children nodes
onready var _animationPlayer = $AnimationPlayer;
onready var _reparationArea = $ReparationArea;
#endregion
	
#region Movement control values
export var _maxSpeed: float = 200;

export var _acceleration: float = 800;

export var _friction: float = 1200;

export var _jumpForce: float = 100;

export var _gravity: float = 1200;
#endregion
	
#region Memorized values
var _velocity = Vector2.ZERO;
var _inputDirection = Vector2.ZERO;
var _willJump = false;
var _fallThrough = false;
var _willAct = false;
var _isRight = true;
var game_running = true;
#endregion

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_animationPlayer.play("IdleRight");
	
func _physics_process(delta: float) -> void:
	_inputDirection.x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left");
	_willJump = Input.is_action_pressed("ui_up") && is_on_floor();
	_willAct = Input.is_action_pressed("act");
	
	set_collision_mask_bit(3, !Input.is_action_pressed("ui_down"));
	
	if (game_running):
		_move(delta);
	
func _move(delta: float) -> void:
	if (_inputDirection.x != 0):
		_velocity.x = _inputDirection.x * min(_maxSpeed, abs(_velocity.x) + _acceleration * delta);
			
		if (_inputDirection.x > 0):
			_isRight = true;
			_animationPlayer.play("RunRight");

		elif (_inputDirection.x < 0):
			_isRight = false;
			_animationPlayer.play("RunLeft");
			
	else:
		_velocity.x = sign(_velocity.x) * max(0, abs(_velocity.x) - _friction * delta);
			
		if (_willAct):
			_willAct = false;
			for area in $ReparationArea.get_overlapping_areas():
				var crack = area as Crack;
				crack.get_repaired();
				
			if (_isRight):
				_animationPlayer.play("HammerRight");
			else:
				_animationPlayer.play("HammerLeft");
				
		elif (_isRight):
			_animationPlayer.play("IdleRight");
		else:
			_animationPlayer.play("IdleLeft");
		
	_velocity.y += _gravity * delta;
	if (_willJump && is_on_floor()):
		_velocity.y = -_jumpForce;
		_willJump = false;
		
	_velocity = move_and_slide(_velocity, Vector2(0, -1));

