extends Area2D
class_name Crack

signal cell_repaired(position);

var cell_pos: Vector2;
var broken = false;

func get_repaired() -> void:
	emit_signal("cell_repaired", cell_pos);
	queue_free();
	
func cracks() -> void:
	broken = true;
	$Sprite.visible = true;
	$Particles2D.emitting = true;
